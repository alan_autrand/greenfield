import unittest
from app import main
from pyramid import testing


class MyTestCase(unittest.TestCase):
    def test_something(self):
        request = testing.DummyRequest()
        response = main(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.text, 'hello world')


if __name__ == '__main__':
    unittest.main()
